#
# Project makefile for a Tango project. You should normally only need to modify
# PROJECT below.
#
CAR_OCI_REGISTRY_HOST=artefact.skao.int

TARANTA=true

# JUPYTER_IMAGE=$(CAR_OCI_REGISTRY_HOST)/ska-tango-examples
JUPYTER_IMAGE=registry.gitlab.com/ska-telescope/sdi/ska-cicd-deploy-low-itf/tango-notebook ## Image for the Jupyter Notebook
# JUPYTER_IMAGE_TAG=0.4.19 ## Jupyter image tag
JUPYTER_IMAGE_TAG=0.1.1
JUPYTER_NAMESPACE ?= jupyter
JUPYTER_VALUES ?= resources/jupyter-config.yaml
#
# CAR_OCI_REGISTRY_HOST and PROJECT are combined to define
# the Docker tag for this project. The definition below inherits the standard
# value for CAR_OCI_REGISTRY_HOST = artefact.skao.int and overwrites
# PROJECT to give a final Docker tag of
# artefact.skao.int/ska-tango-examples/powersupply
#
PROJECT = ska-cicd-deploy-mid-itf

# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= integration-itf

# RELEASE_NAME is the release that all Kubernetes resources will be labelled
# with
RELEASE_NAME ?= test

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART ?= $(PROJECT)
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/

# Fixed variables
# Timeout for gitlab-runner when run locally
TIMEOUT = 86400

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0

CI_PROJECT_PATH_SLUG ?= $(PROJECT)
CI_ENVIRONMENT_SLUG ?= $(PROJECT)

# Settings for the Minikube VMs
ONE_G=1024
CPU?=4

#
# include makefile to pick up the standard Make targets, e.g., 'make build'
# build, 'make push' docker push procedure, etc. The other Make targets
# ('make interactive', 'make test', etc.) are defined in this file.
#

# include OCI Images support
include .make/oci.mk

# include k8s support
include .make/k8s.mk

# include Helm Chart support
include .make/helm.mk

# Include Python support
include .make/python.mk

# include raw support
include .make/raw.mk

# include core make support
include .make/base.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak

# include Git support
# include .make/release.mk

# include Dev support
# include .make/dev.mk

# include useful make targets for working in the ITF
-include ./resources/itf.mk

# Include commands used to speed up development of Test Equipment Tango Devices
-include ./resources/te-dev.mk

# Chart for testing
K8S_CHART = $(PROJECT)
K8S_CHARTS = $(K8S_CHART)

CI_JOB_ID ?= local##pipeline job id
TANGO_HOST ?= tango-databaseds:10000## TANGO_HOST connection to the Tango DS
TANGO_SERVER_PORT ?= 45450## TANGO_SERVER_PORT - fixed listening port for local server
CLUSTER_DOMAIN ?= cluster.local## Domain used for naming Tango Device Servers
K8S_TEST_RUNNER = test-runner-$(CI_JOB_ID)##name of the pod running the k8s-test

ITANGO_ENABLED ?= true## ITango enabled in ska-tango-base

COUNT ?= 1

PYTHON_VARS_AFTER_PYTEST = -m 'not post_deployment' --forked --disable-pytest-warnings --count=$(COUNT)

ifeq ($(strip $(firstword $(MAKECMDGOALS))),k8s-test)
# need to set the PYTHONPATH since the ska-cicd-makefile default definition 
# of it is not OK for the alpine images
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=/app/src:/usr/local/lib/python3.9/site-packages TANGO_HOST=$(TANGO_HOST)
PYTHON_VARS_AFTER_PYTEST := -m 'post_deployment' --disable-pytest-warnings \
	--count=1 --timeout=300 --forked --true-context
endif

HELM_CHARTS_TO_PUBLISH = $(HELM_CHART)
HELM_CHARTS ?= $(HELM_CHARTS_TO_PUBLISH)

PYTHON_BUILD_TYPE = non_tag_setup

PYTHON_SWITCHES_FOR_FLAKE8=--ignore=F401,W503 --max-line-length=180

ifneq ($(CI_REGISTRY),)
K8S_TEST_TANGO_IMAGE = --set tango_example.tango_example.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA) \
	--set tango_example.tango_example.image.registry=$(CI_REGISTRY)/ska-telescope/ska-tango-examples
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/ska-tango-examples/ska-tango-examples:$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
else
K8S_TEST_TANGO_IMAGE = --set tango_example.tango_example.image.tag=$(VERSION)
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-tango-examples:$(VERSION)
endif

TARANTA_PARAMS = --set ska-taranta.enabled=$(TARANTA) \
				 --set ska-taranta-auth.enabled=$(TARANTA) \
				 --set ska-dashboard-repo.enabled=$(TARANTA)

ifneq ($(MINIKUBE),)
ifneq ($(MINIKUBE),true)
TARANTA_PARAMS = --set ska-taranta.enabled=$(TARANTA) \
				 --set ska-taranta-auth.enabled=false \
				 --set ska-dashboard-repo.enabled=false
endif
endif

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.exposeAllDS=$(EXPOSE_All_DS) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	--set global.device_server_port=$(TANGO_SERVER_PORT) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set ska-tango-base.itango.enabled=$(ITANGO_ENABLED) \
	$(TARANTA_PARAMS) \
	# ${K8S_TEST_TANGO_IMAGE} \
	--set event_generator.events_generator.image.tag=$(VERSION)


# override python.mk python-pre-test target
python-pre-test:
	@echo "python-pre-test: running with: $(PYTHON_VARS_BEFORE_PYTEST) $(PYTHON_RUNNER) pytest $(PYTHON_VARS_AFTER_PYTEST) \
	 --cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml --junitxml=build/reports/unit-tests.xml $(PYTHON_TEST_FILE)"

k8s-pre-test: python-pre-test

k8s-pre-template-chart: k8s-pre-install-chart

local-k8s-test: 
	@pytest -m 'post_deployment' --disable-pytest-warnings --count=1 --timeout=300 --forked --true-context  \
		--cov=src --cov-report=term-missing --cov-report xml:build/reports/code-coverage.xml \
		--junitxml=build/reports/unit-tests.xml tests/

requirements: ## Install Dependencies
	poetry install

start_pogo: ## start the pogo application in a docker container; be sure to have the DISPLAY and XAUTHORITY variable not empty.
	docker run --network host --user $(shell id -u):$(shell id -g) --volume="$(PWD):/home/tango/ska-tango-examples" --volume="$(HOME)/.Xauthority:/home/tango/.Xauthority:rw" --env="DISPLAY=$(DISPLAY)" $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-pogo:9.6.35

.PHONY: pipeline_unit_test requirements

jupyter-hub: ## Deploy Jupyter Hub Helm Chart with settings
	helm repo add jupyterhub https://jupyterhub.github.io/helm-chart/;
	helm repo update;
	helm upgrade --cleanup-on-fail \
	--install $(HELM_RELEASE) jupyterhub/jupyterhub \
	--namespace $(JUPYTER_NAMESPACE) \
	--create-namespace \
	--set singleuser.image.name=$(JUPYTER_IMAGE) \
	--set singleuser.image.tag=$(JUPYTER_IMAGE_TAG) \
	--set singleuser.extraEnv.TANGO_HOST=$(TANGO_HOST);
	kubectl get service -n $(JUPYTER_NAMESPACE) proxy-public -o jsonpath="{.status}" | jq;
	. resources/jupyter-haproxy.sh;

install-jupyter-hub: jupyter-hub haproxy-jupyter ## Install Jupyter Hub and HAProxy exposing JHub

haproxy-jupyter:
	make rm-haproxy-jupyter || true
	. resources/jupyter-haproxy.sh

rm-haproxy-jupyter: ## Remove JHub HAProxy
	docker stop jupyter-haproxy
	docker rm jupyter-haproxy

rm-jupyter: ## Uninstall Jupyter Hub
	kubectl delete deployments,statefulsets -n jupyter --all
	kubectl delete namespace jupyter

uninstall-jupyter: rm-haproxy-jupyter rm-jupyter

update-jupyter: rm-jupyter jupyter-hub ## Uninstall and reinstall Jupyterhub chart

go-ahead:
	@false || echo "Make my day"

SLEEP?=1
sleep:
	@[ $(SLEEP) = 1 ] && echo "$(SLEEP) second please..." || echo "$(SLEEP) seconds please..."
	@sleep $(SLEEP)

up:
	@make sleep
	@cd .deploy-minikube && make info
	@minikube status; rc=$$?; \
	if [ $$rc != 0 ]; then \
		cd .deploy-minikube && make all; \
	fi;
	@make sleep
	@make make
	@make
	@cd .deploy-minikube && make minikube-test || true;
	@echo
	@make jupyter-hub

install-all:
	@make k8s-install-chart TARANTA=$(TARANTA)
	@k9s -n $(KUBE_NAMESPACE)

all: up kubeconfig install-all

kubeconfig:
	@kubectl config view --flatten > KUBECONFIG && sudo cp KUBECONFIG /srv/

minikube-stop:
	@echo "Attempting to stop Minikube"
	@cd .deploy-minikube && make clean

down: delete-te minikube-stop

delete-te:
	@make k8s-uninstall-chart && make k8s-delete-namespace || echo "Failed to delete $(KUBE_NAMESPACE) and uninstall $(HELM_RELEASE)"

itango:
	kubectl -n $(KUBE_NAMESPACE) exec --stdin --tty ska-tango-base-itango-console  -- itango3

ngrok-run:
	ngrok http $(shell minikube ip) > /dev/null &


host-ip:
	@curl http://localhost:4040/api/tunnels -s | jq -r .tunnels[0].public_url


ngrok: ngrok-run host-ip

ngrok-stop:
	kill $(shell ps aux | grep [n]grok | awk '{print $$2}')

update-te-image:
	cd .te/ska-ser-test-equipment && eval $$(minikube docker-env) && make oci-build;

reinstall-te: delete-te k8s-install-chart

vars-minikube:
	@cd .deploy-minikube && make minikube-vars
