ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.3.28"
FROM $BUILD_IMAGE AS buildenv
SHELL ["/bin/bash", "-c"]

RUN echo $USER

USER root

# RUN apk --update add --no-cache pkgconfig boost-dev tar telnet openssh-client
RUN poetry config virtualenvs.create false

WORKDIR /app
COPY --chown=tango:tango ./.te/ska-ser-test-equipment /app

# RUN poetry install
# hangs on: • Installing untokenize (0.1.1)
# so use pip instead
RUN poetry export --format requirements.txt --output poetry-requirements.txt --without-hashes && \
    pip install -r poetry-requirements.txt && \
    rm poetry-requirements.txt && pip install .

# USER tango

# packages needed for tunneling
RUN sudo apt-get update
RUN sudo apt-get install telnet openssh-client -y

# Set up SSH for ssh tunneling
RUN sudo mkdir work
COPY .ssh/* /work/.ssh/

# here are some default values (can be overwritten by an env.txt for instance)
ENV SSHKEY=id_rsa
ENV TUNNEL_HOST=127.0.0.1
ENV LOCAL_PORT=5025
ENV REMOTE_HOST=10.20.7.1
ENV REMOTE_PORT=5025


