# Mid System ITF deployment infrastructure

This repository is for setting up a minimal cluster for using SKA Software, deployment of and connecting to Tango-controlled Test Equipment at the MID System ITF.

At a later stage, this repository should be used for setting up jump hosts, Ceph, Elasticstack, Prometheus, Grafana, Gitlab CICD, and fully-fledged Kubernetes clusters.

## Getting started
Clone the repo
```
$ git clone --recurse-submodules git@gitlab.com:ska-telescope/sdi/ska-cicd-deploy-mid-itf.git
```
# Usage
## Using make commands
If you want to know what a `make` command is going to do, run it with `--dry` postfixed. For instance, if you just run `make go-ahead`, the output is just "Make my day". But if you ran `make go-ahead --dry`, you'll notice that the command first does a boolean test which fails immediately (false is a failure), and as a result it runs the `echo "Make my day"` bit:
```
$ make go-ahead --dry
false || echo "Make my day"
$ make go-ahead
Make my day
```

## Preparing your PrivateRules.mak file
In the root of the project, you can add any Makefile variables or commands that you may prefer, to a file called `PrivateRules.mak` (you need to create this file yourself). It is ignored by Git so it will not override your co-workers' settings.

## Running Minikube and the Test Equipment with one command
Two special Make targets are included in this repo to carry out all the steps for deployment of a Minikube cluster given that all the Prerequisites are installed.

BE CAREFUL: `make up` also runs `make copy-private-rules`, which is a destructive command that overrides the `PrivateRules.mak` file inside the `.deploy-minikube` directory with the one residing in your repo root.
At the moment `make up` is less destructive by default thanks to the make variable NO_COPY=True being set in this project's root `PrivateRules.mak`. This will therefore only affect you if you've unset that variable here.

```
make up
```
and
```
make down
```
Use the tips from the previous section to learn what those commands do. Nice, eh?

## Provisioning a cluster
- [ ] update once central server is available - remove Minikube stuff or add cluster provisioning details.

### ITF "The-Beast":
The Beast is an interim VM used for Software deployments at the ITF. The VM hosts a Minikube cluster that is provisioned using the Makefile as described above.

For local deployment (on a laptop), using the SKA Deploy Minikube repository is recommended. This repo might also be useful at the System ITF and so the pinned, working version of the Minikube repo is added as a Git Submodule of this project. Follow the [README](https://gitlab.com/ska-telescope/sdi/ska-cicd-deploy-minikube/-/blob/master/README.md) - this can also be opened using VSCode and a Markdown Preview extension such as [Preview](https://marketplace.visualstudio.com/items?itemName=searKing.preview-vscode).

For a quick setup of a cluster (assuming all your Prerequisites are met - see installation details of all these things in the [Confluence page describing them]()), do:
```
cd .deploy-minikube
make make ## update your Makefile tooling to the latest
make ## just to check your variables
make all ## INSTALL ALL THE THINGS AND RUN MINIKUBE
```

## Deploying all the things
"Skampi software" is deployable through this repository, using the standardised Helm deployment tooling. We have a special `make up` target that spins up Minikube, installs Spookd (see below), then installs the Test Equipment chart and then SKAMPI (the last step is not yet included and will likely change in PI16). `make down` obviously tears down the cluster.

Installing Jupyterhub is a separate step that (currently) also requires a haproxy set up, and is done like so:
```
$ make install-jupyter-hub # to install
# make uninstall-jupyter # to uninstall
```
Alternatively the haproxy can be separately controlled - use `make <target> --dry` to learn more.

## Enabling kubernetes cluster access
Get the KUBECONFIG file stored in a folder where all can access it:
```
make kubeconfig
```

Control your deployment through the `values.yaml` file, and follow the examples set in SKAMPI. **Please ensure that your version numbers are up to date!**

*Note that the `Chart.yaml` file currently doesn't contain the `ska-mid` chart, because Helm downloads all the charts first and then notices that most of the things are "switched off" (meaning marked enabled=false in the `values.yaml` file) - this wastes time unnecessarily.*

## Make Test Equipment exclusively available to a singla pod in a cluster using Spookd

See these [Test Steps for setting up the Signal Generator](https://jira.skatelescope.org/browse/XTP-9433). Test definition may have to be modified slightly.

## Detailed instructions for accessing (currently the interim server) and using the Test Equipment Taranta Dashboards
Please visit https://confluence.skatelescope.org/display/SE/Accessing+ITF+network+based+Test+Equipment+and+SUT to learn about the current deployment setup.

### TL;DR links
* Go to [this section on Taranta Dashboards](https://confluence.skatelescope.org/display/SE/Accessing+ITF+network+based+Test+Equipment+and+SUT#AccessingITFnetworkbasedTestEquipmentandSUT-AccessTarantadashboardsinthebrowserfromaDeveloperLaptop(C1/C2)).
* Check [this section about running `make python-test` at the ITF against real test hardware](https://confluence.skatelescope.org/display/SE/Accessing+ITF+network+based+Test+Equipment+and+SUT#AccessingITFnetworkbasedTestEquipmentandSUT-Runpython-testfromacontaineragainstrealhardware).
* 

