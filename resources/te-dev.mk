te-build:
	@eval $$(minikube docker-env)
	@cd .te/ska-ser-test-equipment
	@ls -latr
	@make oci-build

te-remove: #remove currently running Test Equipment deployment
	