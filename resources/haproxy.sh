#!/bin/sh

cat <<EOF > haproxy.cfg
frontend front-https-proxy
  bind *:8080
  mode tcp
  option forwardfor
  default_backend back-https-proxy

backend back-https-proxy
  mode tcp
  balance roundrobin
  server server1 192.168.49.96:80 check
EOF

# now lets go!
docker run --name haproxy --net=host \
       -v $(pwd)/haproxy.cfg:/usr/local/etc/haproxy/haproxy.cfg \
       -d haproxy:2.6 -f /usr/local/etc/haproxy/haproxy.cfg
